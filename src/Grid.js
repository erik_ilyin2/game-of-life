import React, { Component } from 'react';
import Cell from './Cell';

export default class Grid extends Component {
  componentWillMount() {
    this.isDown = false
    this.select = true
  }

  onCellOver = (row, col) => {
    
    if(this.isDown)
      this.props.onSelectCell(row, col, this.select)
  }

  onCellDown = (row, col) => {
    this.isDown = true
    const {data, onSelectCell} = this.props
    this.select = !data[row][col]
    onSelectCell(row, col, this.select)
  }

  onCellUp = () => {
    this.isDown = false
  }

  render() {
    const { cellSize, generation, rows, cols, data } = this.props;
    return (
      <svg
        width={cols*cellSize}
        height={rows*cellSize}
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
      >
        { data.map((row, i) =>
          row.map((cell, j) =>
            <Cell
              generation = {generation}
              onOver={ this.onCellOver }
              onDown={ this.onCellDown }
              onUp= { this.onCellUp }
              selected = {data[i][j]}
              cellSize={ cellSize }
              row={i}
              col={j}
              key={ i + "-" + j }
            />
          )
        ) }
      </svg>
    );
  }
}
