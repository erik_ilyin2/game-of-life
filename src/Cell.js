import React, { Component } from 'react';
const colors = ['white','lightgray','green']
export default class Cell extends Component {
    
    componentWillMount() {
      this.setState ({ status: 0, generation: 0 });
      console.log("mount")
    }
    componentWillReceiveProps(newProps) {
      const {status, generation} = this.state
      var newStatus = 0
      if(newProps.selected) {
        newStatus = 2
      } else {
        if((status >= 1 && generation<=newProps.generation ) )
          newStatus = 1
        else
          newStatus = 0
      }
      this.setState({status: newStatus, generation: newProps.generation})
    }

    onMouseMove = (event) => {
      this.props.onOver(this.props.row, this.props.col);
    }

    onMouseDown = (event) => {
      this.props.onDown(this.props.row, this.props.col);
    }
    render() {
        const {row, col, cellSize, onDown, onUp} = this.props
        return (
        <rect
          onMouseMove={this.onMouseMove}
          onMouseDown = {this.onMouseDown}
          onMouseUp = {onUp}
          x={ col*cellSize }
          y={ row*cellSize }
          width={ cellSize }
          height={ cellSize }
          fill={ colors[this.state.status] }
          stroke="#aaaaaa"
          strokeWidth="1"
        />
        );
    }
}
