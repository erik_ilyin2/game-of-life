import React, { Component } from 'react';
import logo from './logo.svg';
import Grid from './Grid';
import './App.css';
const ROWS_COUNT = 25
const COLS_COUNT = 50
class App extends Component {

  emptyGrid() {
    var matrix = new Array(ROWS_COUNT);
    for (var i = 0; i < ROWS_COUNT; i++) {
      matrix[i] = new Array(COLS_COUNT);
      for (var j = 0; j < COLS_COUNT; j++) {
        matrix[i][j] = false;
      }
    }
    return matrix;
  }

  componentWillMount() {
    var grid = this.emptyGrid()
    this.setState({
      generation: 0,
      grid,
    });
  }

  liveNeighborsCount(row, col) {
    var result = 0;
    for (var i = -1; i <= 1; i++) {
      for (var j = -1; j <= 1; j ++) {
        if (!(i === 0 && j === 0) && this.cellInBounds(row+i, col+j) && this.state.grid[row+i][col+j])
          result = result + 1
      }
    }
    return result;
  }

  cellInBounds(row, col) {
    return (row>=0 && row<ROWS_COUNT-1) && (col>=0 && col<COLS_COUNT-1);
  }

  generateStep (oldGrid) {
    var newGrid = this.emptyGrid();
    for (var row = 0; row < ROWS_COUNT; row++) {
      for (var col = 0; col < COLS_COUNT; col++) {
        var liveCount = this.liveNeighborsCount(row, col);
        if (oldGrid[row][col]) {
          newGrid[row][col] = (liveCount === 3 || liveCount === 2);
        } else {
          newGrid[row][col] = (liveCount === 3);
        }
      }
    }
    return newGrid;
  }

  onSelectCell = (row, col, select=true) => {
    const {grid} = this.state
    grid[row][col] = select
    this.setState({grid: [...grid]})
  }

  onTimer = () => {
    var oldGrid = this.state.grid;
    var grid = this.generateStep(oldGrid);
    this.setState({
      generation : this.state.generation + 1,
      grid
    });
  }

  onStart = () => {
    if(this.timerId)
      clearInterval(this.timerId)
    this.timerId = setInterval(this.onTimer, 500)
  }

  onStop = () => {
    var grid = this.emptyGrid()
    if(this.timerId)
      clearInterval(this.timerId)
    this.setState({
      generation: 0,
      grid,
    })
  }
  

  onPause = () => {
    if(this.timerId)
      clearInterval(this.timerId)
  }

  
  render() {
    const {generation, grid} = this.state
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Generation : {generation}</h1>
        </header>
        <div className="row">
          <Grid
            onSelectCell={this.onSelectCell}
            data={grid}
            rows={ROWS_COUNT}
            cols={COLS_COUNT}
            cellSize={20}
            generation = {generation}
          />
        </div>
        <div className="row">
          <button onClick={this.onStart}>
            Start
          </button>
          <button onClick={this.onPause}>
            Pause
          </button>
          <button onClick={this.onStop}>
            Stop
          </button>
        </div>
      </div>
    );
  }
}

export default App;
